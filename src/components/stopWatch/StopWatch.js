import { Component } from "react";
import "./StopWatch.css";


class StopWatch extends Component{
    state= {timer:0,min:0}
    
    tickClock = ()=>{
        this.timerId = setInterval(()=>{
            let {timer,min} = this.state 
            if(timer >= 59){
                this.setState(()=>({
                    timer:0,
                    min:min+=1
                }))
            }else{
                this.setState({timer:timer+=1, min:min})
            }
            
        },1000)
    }

    restartTimer = ()=>{
        clearInterval(this.timerId)
        this.setState({timer:0,min:0})
    }


    clearTimer = ()=>{
        clearInterval(this.timerId)
    }


    render(){
        const {timer,min} = this.state;
        
        return(
            <div className = "bg-container">
                <h1 className="stopWatchHeader">STOP WATCH</h1>
                <p className="timer">{timer === 59 ? `${min <= 9 ? `0${min}`:min } : ${timer}` :`${min <=9 ?`0${min}`:min } : ${timer<=9 ? `0${timer}`:timer}`}</p>
                <div className="button-container">
                    <button className="start buttons" onClick={this.tickClock}>Start</button>
                    <button className="stop buttons" onClick={this.clearTimer}>Stop</button>
                    <button className="restart buttons" onClick={this.restartTimer}>Restart</button>
                </div>

            </div>
        )
    }
}

export default StopWatch